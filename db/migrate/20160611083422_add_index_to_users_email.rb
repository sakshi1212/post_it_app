class AddIndexToUsersEmail < ActiveRecord::Migration
  def change
  	add_index :users, :email, unique: true
  	# add index to users table , email coloum and make the index unique
  end
end
