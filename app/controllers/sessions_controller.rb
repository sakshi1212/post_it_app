class SessionsController < ApplicationController
  
  #  get 'login' => 'sessions#new'
  # post 'login' => 'session#create'
  # delete 'logout' => 'session#destroy'

  def new
  end

  def create
  	user = User.find_by(email: params[:session][:email].downcase)
  	if user && user.authenticate(params[:session][:password])
  		# before email authentication
      # log_in user
      #   params[:session][:remember_me] == 1? remember(user) : forget(user)
  		# redirect_to user # user = user_url(user)
      if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        redirect_back_or user
      else
        message  = "Account not activated. "
        message += "Check your email for the activation link."
        flash[:warning] = message
        redirect_to root_url
      end
  	else
  		flash.now[:danger] = "Invalid email/password conbination" #can improve
  		render 'new'
  	end
  end

  def destroy
  	logout if logged_in?
  	redirect_to root_url
  end


end
