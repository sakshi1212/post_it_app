require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  
  def setup
    @user = users(:sakshi)
    @other_user = users(:deeksha)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

   # tests giving error -- scheme error http does not accept registry part ??? -- fix later
    #invalid URI error
  test "should redirect edit when not logged in" do
    get :edit, id: @user
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do
    patch :update, id: @user, user: {name: @user.name, email: @user.email}
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect edit when logged in as wrong user" do
  	log_in_as(@other_user)
  	get :edit, id: @user
  	assert flash.empty?
  	assert_redirected_to root_url  	
  end

  test "should redirect update when logged in as wrong user" do
  	log_in_as(@other_user)
  	patch :update, id: @user, user: {email: @user.email, email: @user.email}
  	assert flash.empty?
  	assert_redirected_to root_url
  end

  # show all users can be viewed by non logged in users
  # index can only be viewed by logged in users

  test "should redirect index when not logged in" do
  	get :index
  	assert_redirected_to login_url
  end

end
