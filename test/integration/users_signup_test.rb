require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  test "invalid signup information" do
  	get signup_path
  	assert_no_difference 'User.count' do
  		post users_path, user: {name: "",
  								email: "invalid@invalid", 
  								password: "wrong",
  								password_confirmation: "wront"}
  	end
  	# can also use the below lines instead of assert_no_difference 
	 #  before_count = User.count
	# post users_path, ...
	# after_count  = User.count
	# assert_equal before_count, after_count
	assert_template 'users/new'
   end

   test "valid signup information" do
   	get signup_path
   	assert_difference "User.count", 1 do
   		post_via_redirect users_path, user: {name: "XYZ", 
   											email: "xyz@xyz.com", 
   											password: "abcdef", 
   											password_confirmation: "abcdef"}
   	end
   	assert_template 'users/show'
    assert is_logged_in?
   end

end
