require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup
    @user = users(:sakshi)
  end

  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), user: { name:  '',
                                    email: 'invalid@invalid',
                                    password: 'wrong',
                                    password_confirmation: 'wront' }
    assert_template 'users/edit'
  end

  test "successful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    name  = "Sakshi Kakkar"
    email = "skakkar121291@gmail.com"
    patch user_path(@user), user: { name:  name,
                                    email: email,
                                    password: "password",
                                    password_confirmation: "password" }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal @user.name,  name
    assert_equal @user.email, email
  end

 


end
